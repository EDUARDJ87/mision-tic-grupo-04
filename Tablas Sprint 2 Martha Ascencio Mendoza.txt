
CREATE TABLE IF NOT EXISTS consultorio(
idconsultorio INT NOT NULL AUTO_INCREMENT,
id_sede INT (11) NOT NULL,
nombre VARCHAR (45) NOT NULL,
PRIMARY KEY (idconsultorio),
CONSTRAINT fk_consultorio_sede FOREIGN KEY (id_sede) REFERENCES sede(idsede)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS agenda(
idagenda INT NOT NULL AUTO_INCREMENT,
horainicio DATETIME NOT NULL,
horafin DATETIME NOT NULL,
disponibilidad TINYINT (4) NOT NULL,
id_consultorio INT (11) NOT NULL,
tiempo_cita INT (11) NOT NULL,
PRIMARY KEY (idagenda),
CONSTRAINT fk_agenda_consultorio FOREIGN KEY (id_consultorio) REFERENCES consultorio(idconsultorio)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS cita(
idcita INT NOT NULL AUTO_INCREMENT,
id_paciente INT (11) NOT NULL,
id_doctor INT (11) NOT NULL,
hora_inicio_cita DATETIME NOT NULL,
hora_fin_cita  DATETIME NOT NULL,
hora_inicio_atencion DATETIME NOT NULL,
hora_fin_atencion DATETIME NOT NULL,
estado VARCHAR(45),
id_agenda INT (11) NOT NULL
PRIMARY KEY (idcita),
CONSTRAINT fk_cita_paciente FOREIGN KEY (id_paciente) REFERENCES paciente (idpaciente),
CONSTRAINT fk_cita_usuario FOREIGN  KEY (id_doctor) REFERENCES usuario (idusuario),
CONSTRAINT fk_cita_agenda FOREIGN KEY (id_agenda) REFERENCES agenda (idagenda)
) ENGINE = INNODB;