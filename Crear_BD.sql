CREATE DATABASE `consultorio`;
USE consultorio;

CREATE TABLE `persona` (
  `idpersona` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `identificacion` varchar(45) NOT NULL,
  `typoidentificacion` varchar(45) NOT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `activo` tinyint(4) NOT NULL,
  PRIMARY KEY (`idpersona`)
) ENGINE=InnoDB;

CREATE TABLE `eps` (
  `ideps` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `nit` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ideps`)
) ENGINE=InnoDB;

CREATE TABLE `paciente` (
  `idpaciente` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` int(11) NOT NULL,
  `id_eps` int(11) NOT NULL,
  PRIMARY KEY (`idpaciente`),
  KEY `paciente_id_persona_idx` (`id_persona`),
  KEY `paciente_id_eps_idx` (`id_eps`),
  CONSTRAINT `paciente_id_eps` FOREIGN KEY (`id_eps`) REFERENCES `eps` (`ideps`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `paciente_id_persona` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `role` (
  `idrole` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`idrole`)
) ENGINE=InnoDB;

CREATE TABLE `especialidad` (
  `idespecialidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`idespecialidad`)
) ENGINE=InnoDB;

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `contrasena` varchar(45) NOT NULL,
  `id_person` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_especialidad` int(11) DEFAULT NULL,
  `registro_medico` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `user_person_id_idx` (`id_person`),
  KEY `userroleid_idx` (`id_role`),
  KEY `user_especialidad_id_idx` (`id_especialidad`),
  CONSTRAINT `user_especialidad_id` FOREIGN KEY (`id_especialidad`) REFERENCES `especialidad` (`idespecialidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userpersonid` FOREIGN KEY (`id_person`) REFERENCES `persona` (`idpersona`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `userroleid` FOREIGN KEY (`id_role`) REFERENCES `role` (`idrole`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `institucion` (
  `idinstitucion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `slogan` varchar(45) DEFAULT NULL,
  `logo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idinstitucion`)
) ENGINE=InnoDB;

CREATE TABLE `user_id_institution_id` (
  `user_id` int(11) NOT NULL,
  `institution_id` int(11) NOT NULL,
  KEY `user_id_idx` (`user_id`),
  KEY `institution_id_idx` (`institution_id`),
  CONSTRAINT `institution_id` FOREIGN KEY (`institution_id`) REFERENCES `institucion` (`idinstitucion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `sede` (
  `idsede` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `id_institution` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsede`),
  KEY `sedeidinstitution_idx` (`id_institution`),
  CONSTRAINT `sedeidinstitution` FOREIGN KEY (`id_institution`) REFERENCES `institucion` (`idinstitucion`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `consultorio` (
  `idconsultorio` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `id_sede` int(11) DEFAULT NULL,
  PRIMARY KEY (`idconsultorio`),
  KEY `consultorio_id_sede_idx` (`id_sede`),
  CONSTRAINT `consultorio_id_sede` FOREIGN KEY (`id_sede`) REFERENCES `sede` (`idsede`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `agenda` (
  `idagenda` int(11) NOT NULL AUTO_INCREMENT,
  `disponibilidad` tinyint(4) NOT NULL,
  `id_consultorio` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`idagenda`),
  KEY `agenda_id_consultorio_idx` (`id_consultorio`),
  KEY `ageda_id_user_idx` (`id_user`),
  CONSTRAINT `ageda_id_user` FOREIGN KEY (`id_user`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `agenda_id_consultorio` FOREIGN KEY (`id_consultorio`) REFERENCES `consultorio` (`idconsultorio`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `franja` (
  `idfranja` int(11) NOT NULL AUTO_INCREMENT,
  `horainicio` date NOT NULL,
  `horafin` date NOT NULL,
  `disponibilidad` tinyint(4) NOT NULL,
  `id_agenda` int(11) NOT NULL,
  PRIMARY KEY (`idfranja`),
  KEY `franja_idx` (`id_agenda`),
  CONSTRAINT `franja` FOREIGN KEY (`id_agenda`) REFERENCES `agenda` (`idagenda`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `medicamentos` (
  `idmedicamentos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  `presentacion` varchar(45) NOT NULL,
  `unidad` varchar(45) NOT NULL,
  PRIMARY KEY (`idmedicamentos`)
) ENGINE=InnoDB;



CREATE TABLE `historiaclinica` (
  `idhistoriaclinica` int(11) NOT NULL AUTO_INCREMENT,
  `analisis_tratamiento` longtext NOT NULL,
  `motivo` longtext NOT NULL,
  `diagnostico` longtext NOT NULL,
  PRIMARY KEY (`idhistoriaclinica`)
) ENGINE=InnoDB;

CREATE TABLE `medicamentosformulados` (
  `idmedicamentosformulados` int(11) NOT NULL AUTO_INCREMENT,
  `id_medicamento` int(11) NOT NULL,
  `id_historia_clinica` int(11) NOT NULL,
  `dosis` varchar(45) NOT NULL,
  `tiempo` varchar(45) NOT NULL,
  `dias` varchar(45) NOT NULL,
  `cantidad` varchar(45) NOT NULL,
  `nota` varchar(45) NOT NULL,
  PRIMARY KEY (`idmedicamentosformulados`),
  KEY `formulados_id_medicamentos_idx` (`id_medicamento`),
  KEY `formulados_id_historiaclinica_idx` (`id_historia_clinica`),
  CONSTRAINT `formulados_id_historiaclinica` FOREIGN KEY (`id_historia_clinica`) REFERENCES `historiaclinica` (`idhistoriaclinica`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `formulados_id_medicamentos` FOREIGN KEY (`id_medicamento`) REFERENCES `medicamentos` (`idmedicamentos`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `cita` (
  `idcita` int(11) NOT NULL,
  `id_paciente` int(11) NOT NULL,
  `id_doctor` int(11) NOT NULL,
  `hora_inicio_atencion` datetime NOT NULL,
  `hora_fin_atencion` datetime NOT NULL,
  `estado` varchar(45) NOT NULL,
  `id_franja` int(11) NOT NULL,
  `id_historia_clinica` int(11) NOT NULL,
  PRIMARY KEY (`idcita`),
  KEY `cita_id_paciente_idx` (`id_paciente`),
  KEY `cita_id_doctor_idx` (`id_doctor`),
  KEY `cita_id_franja_idx` (`id_franja`),
  KEY `cita_id_historia_clinica_idx` (`id_historia_clinica`),
  CONSTRAINT `cita_id_doctor` FOREIGN KEY (`id_doctor`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cita_id_franja` FOREIGN KEY (`id_franja`) REFERENCES `franja` (`idfranja`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cita_id_historia_clinica` FOREIGN KEY (`id_historia_clinica`) REFERENCES `historiaclinica` (`idhistoriaclinica`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cita_id_paciente` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`idpaciente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;







