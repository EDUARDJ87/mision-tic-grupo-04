
CREATE TABLE IF NOT EXISTS persona(
	idpersona INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(120) NOT NULL,
    identificacion VARCHAR(45) NOT NULL,
    tipoidentificacion VARCHAR(45) NOT NULL,
    correo VARCHAR(100),
    telefono VARCHAR(45),
    PRIMARY KEY (idpersona)
)ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS eps(
ideps INT NOT NULL AUTO_INCREMENT,
nombre VARCHAR (45),
nit VARCHAR(45),
PRIMARY KEY (ideps)
)ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS paciente(
idpaciente INT NOT NULL,
idpersona INT NOT NULL,
ideps INT NOT NULL,
PRIMARY KEY (idpaciente),
CONSTRAINT fk_persona_persona_id FOREIGN KEY (idpersona) REFERENCES persona (idpersona),
CONSTRAINT fk_pacientes FOREIGN KEY (ideps) REFERENCES eps (ideps)
)ENGINE=INNODB;